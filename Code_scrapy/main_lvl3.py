from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import mysql.connector

# Initialisation de la liaison avec le site à scraper
url = "http://localhost:8080/level3/"
page = urlopen(url)
html = page.read().decode("utf-8")
document = BeautifulSoup(html, "html.parser")

# Initialisation de la liaison avec la BDD à implémenter
mydb = mysql.connector.connect(
  host="localhost",
  user="antony",
  password="choupette",
  auth_plugin='mysql_native_password',
  database="scrapy"
)
    #Inite du curseur de la BDD
mycursor = mydb.cursor()

# Code BS4 pour un tableau HTML:
def formate_table(list):
    formate_list = []
    for i in list:
        if i != "\n":
            formate_list.append(i.get_text('\n',strip=True))
    return formate_list

    # Var_BS4 pour la version HTML
all_products_HTML = document.select("table tbody tr")
    # Var pour le tableau
all_products_dict = []

# Récupération de key du dictionnaire
key_name = document.select("tr th")[0].text # Name
key_price = document.select("tr th")[1].text # Price
key_desc = document.select("tr th")[2].text # description
key_ref = "reference"                        # reference

# Récupération des valeurs pour les keys
    # Boucle FOR
for prod_HTML in all_products_HTML:
        # Var pour le dictionnaire
    product = {}
        # Var liste pour ref et nom
    name_list = formate_table(prod_HTML.select("tr td")[0].contents)
        # Var pour le nom
    product[key_name] = name_list[1]
        # Var pour le prix réduit
    product[key_price] = formate_table(prod_HTML.select("tr td")[1])[0]
        # Var pour la description
    product[key_desc] = formate_table(prod_HTML.select("tr td")[2])[0]
        # Ajouts du dictionnaire dans le tableau
    product[key_ref] = name_list[0]
    all_products_dict.append(product)

# print(key_name, key_price, key_desc, key_ref) # Les keys de table_3

# Code - Création base de donnée SQL:
    # I - Liste des commandes SQL init
        # Vérification et suppression d'une table existante
table_exist_sql = "DROP TABLE IF EXISTS level_3;"
mycursor.execute(table_exist_sql)
print("Ancienne table level_3: Supprimé")
        # Création de la nouvelle table level_3
table_create_sql = """CREATE TABLE level_3 (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    Name VARCHAR(255),
    reference VARCHAR(255),
    description text,
    Price FLOAT,
    Diff_Price FLOAT
    );"""
mycursor.execute(table_create_sql)
print("Nouvelle table level_3: Créé")

        # Initialisation de la table 2 pour récupération des données
table_2_sql = "SELECT * FROM level_2"
mycursor.execute(table_2_sql)
table_2 = mycursor.fetchall() # Récupère les données et renvoi la table sous forme de liste en 2 dimensions
print("Import table level_2: Stocker dans liste")

        # Transforme la liste de tuple en liste de dictionnaire
list_ref = [] # Table_2 sous forme de dict
for dict in table_2:
    dico = {}
    dico["reference"] = dict[2]
    dico["price"] = dict[4]
    list_ref.append(dico)

    # II - Via connecteur SQL
# INSERT INTO level_1 (col1, col2, ...) VALUES('valeur1', 'valeur2', ...);

        # Parcours de la liste "tous les produits dans dictionnaire"
for item in all_products_dict:
    for ref in list_ref:
        if ref['reference'] == item["reference"]:
            diff_price = float(item["Price"][:-1]) - ref["price"]
            break
    item["Diff_Price"] = diff_price
    sql = "INSERT INTO level_3 (Name, reference, description, Price, Diff_Price) VALUES (%s, %s, %s, %s, %s)"
    val = (item["Name"], item["reference"], item["description"], item["Price"][:-1], item["Diff_Price"] )
    mycursor.execute(sql, val)

print(len(all_products_dict), "record inserted.")

        # Commit du curseur
mydb.commit()


# Code exemple:
#reference_1 = document.select("tr td span")[0].text # méthode 1
#reference_2 = document.select("tr td")[0].contents[1].text # méthode 2
#ref = document.select("tr td span")[0].text # méthode 3
#nom = document.select("tr td")[0].contents[2] # méthode 1

# diff_val = 5
# reference_val = "946282533-5"
# for item in all_products_dict:
#     diff_price_sql = "UPDATE level_3 SET Diff_Price = (%s) WHERE reference = (%s)"
#     reference_val = item["reference"]
#     val_sql = (diff_val, reference_val)
#     mycursor.execute(diff_price_sql, val_sql)