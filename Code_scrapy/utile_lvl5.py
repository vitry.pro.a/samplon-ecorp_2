from bs4 import BeautifulSoup
from urllib.request import urlopen

# Initialisation de la liaison avec le site à scraper
def getDOM(url):
    page = urlopen(url)
    html = page.read().decode("utf-8")
    return BeautifulSoup(html, "html.parser")

# Formatage de la liste pour retrait \n et élément vide
def formate_table(list):
    formate_list = []
    for i in list:
        if i != "\n":
            formate_list.append(i.get_text('\n',strip=True))
    return formate_list

# Récupération des valeurs dans la page HTML
def scrap_page(page_HTML):
    page_products_dict = []
    for prod_HTML in page_HTML:
            # Var pour le dictionnaire
        product = {}
            # Var pour le nom
        product["name"] = prod_HTML.select(".product-info h2")[0].text
        #     # Var pour la description
        product["description"] = prod_HTML.select(".product-info p")[0].text
        #     # Ajouts du dictionnaire dans le tableau
        product["reference"] = prod_HTML.select(".product-info small")[0].text
        #         # Var pour le prix réduit
        product["price"] = (prod_HTML.select(".product-info div.line strong.price")[0].text)[:-1]
        page_products_dict.append(product)
    return page_products_dict