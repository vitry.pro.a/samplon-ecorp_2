DROP DATABASE IF EXISTS scrapy;

CREATE DATABASE scrapy;

USE scrapy;

DROP TABLE IF EXISTS level_1;
DROP TABLE IF EXISTS level_2;

CREATE TABLE level_1 (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    nom VARCHAR(255), 
    ref VARCHAR(255), 
    description text, 
    prix INT
    );

CREATE TABLE level_2 (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    name VARCHAR(255), 
    ref VARCHAR(255), 
    description text, 
    price INT, 
    price_less INT, 
    discount INT
    );