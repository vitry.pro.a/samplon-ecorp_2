from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import mysql.connector
import utile_lvl5

# Initialisation de la liaison avec le site à scraper
DOM_level5 = utile_lvl5.getDOM("http://localhost:8080/level5/0/")

# Initialisation de la liaison avec la BDD à implémenter
mydb = mysql.connector.connect(
  host="localhost",
  user="antony",
  password="choupette",
  auth_plugin='mysql_native_password',
  database="scrapy"
)
# Initialisation du curseur de la BDD
mycursor = mydb.cursor()

# Var_BS4 pour la version HTML
page_products_HTML = DOM_level5.select("div.product")

# Var_BS4 et appel fonction de scrap de page
page_products_dict = utile_lvl5.scrap_page(page_products_HTML)

# Code - Création base de donnée SQL:
    # I - Liste des commandes SQL init
        # Vérification et suppression d'une table existante
table_exist_sql = "DROP TABLE IF EXISTS level_5;"
mycursor.execute(table_exist_sql)
print("Ancienne table level_5: Supprimé")
        # Création de la nouvelle table level_5
table_create_sql = """CREATE TABLE level_5 (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    name VARCHAR(255),
    reference VARCHAR(255),
    description text,
    price FLOAT
    );"""
mycursor.execute(table_create_sql)
print("Nouvelle table level_5: Créé")

    # II - Via connecteur SQL
        # Parcours de la liste "tous les produits dans dictionnaire"
for item in page_products_dict:
    sql = "INSERT INTO level_5 (name, reference, description, price) VALUES (%s, %s, %s, %s)"
    val = (item["name"], item["reference"], item["description"], item["price"])
    mycursor.execute(sql, val)

print(len(page_products_dict), "record inserted.")

        # Commit du curseur
mydb.commit()