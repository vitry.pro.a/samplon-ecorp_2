from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import pandas as pd



url = "http://localhost:8080/level1/"
page = urlopen(url)
html = page.read().decode("utf-8")
document = BeautifulSoup(html, "html.parser")

# Méthode:
    # I - Chercher les sélecteurs CSS
    # II - Lister les variables
    # III - Inclures les commandes .select dans les varibles
    # les bon sélecteurs CSS
    # IV - Inclures les variables dans un dictionnaire
    # V - Faire une boucle pour lister les différents éléments (prod_HTML)

# Code BS4:
    # Var_BS4 pour la version HTML
all_products_HTML = document.select("div.product")

    # Var pour le tableau
all_products_dict = []

    # Boucle FOR
for prod_HTML in all_products_HTML:
        # Var pour le dictionnaire
    produit = {}
        # Var pour le nom
    produit["nom"] = prod_HTML.select(".product-info h2")[0].text
        # Var pour la ref
    produit["ref"] = prod_HTML.select(".product-info small")[0].text
        # Var pour la description
    produit["description"] = prod_HTML.select(".product-info p")[0].text
        # Var pour le prix
    produit["prix"] = prod_HTML.select(".product-info .line strong.price")[0].text
        # Ajouts du dictionnaire dans le tableau
    all_products_dict.append(produit)

# Code - Création base de donnée SQL:
    # I - Liste des commandes SQL init
            # via fichier SQL

    # II - Via utilisation de pandas pour les requètes SQL

pprint(all_products_dict[0].keys())


pd_all_products = pd.DataFrame(all_products_dict)

print(pd_all_products)