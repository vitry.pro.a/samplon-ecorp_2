from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import mysql.connector

# Initialisation de la liaison avec le site à scraper
url = "http://localhost:8080/level2/"
page = urlopen(url)
html = page.read().decode("utf-8")
document = BeautifulSoup(html, "html.parser")

# Initialisation de la liaison avec la BDD à implémenter
mydb = mysql.connector.connect(
  host="localhost",
  user="antony",
  password="choupette",
  auth_plugin='mysql_native_password',
  database="scrapy"
)
    #Inite du curseur de la BDD
mycursor = mydb.cursor()


# Code BS4:
    # Var_BS4 pour la version HTML
all_products_HTML = document.select("div.product")

    # Var pour le tableau
all_products_dict = []

    # Boucle FOR
for prod_HTML in all_products_HTML:
        # Var pour le dictionnaire
    produit = {}
        # Var pour le nom
    produit["name"] = prod_HTML.select(".product-info h2")[0].text
        # Var pour la ref
    produit["ref"] = prod_HTML.select(".product-info small")[0].text
        # Var pour la description
    produit["description"] = prod_HTML.select(".product-info p")[0].text
        # Var pour le prix réduit
    produit["price"] = prod_HTML.select(".product-info .line strong.price")[0].text
        # Var pour le prix initial
    produit["price_less"] = prod_HTML.select(".product-info .line strong.price")[1].text
        # Ajouts du dictionnaire dans le tableau
    produit["discount"] = 0
    all_products_dict.append(produit)

# Code - Création base de donnée SQL:
    # I - Liste des commandes SQL init
            # via fichier SQL

        # Vérification et suppression d'une table existante
table_exist_sql = "DROP TABLE IF EXISTS level_2;"
mycursor.execute(table_exist_sql)
print("Ancienne table level_2: Supprimé")
        # Création de la nouvelle table level_2
table_create_sql = '''CREATE TABLE level_2 (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255), 
    ref VARCHAR(255), 
    description text, 
    price FLOAT, 
    price_less FLOAT, 
    discount FLOAT
    );'''
mycursor.execute(table_create_sql)
print("Nouvelle table level_2: Créé")

    # II - Via connecteur SQL
# INSERT INTO level_1 (col1, col2, ...) VALUES('valeur1', 'valeur2', ...);
def discount(less_price: float, init_price: float):
    return ((1 - (less_price / init_price)) * 100)

        # Parcours de la liste "tous les produits dans dictionnaire"
for item in all_products_dict:
    sql = "INSERT INTO level_2 (name, ref, description, price_less, price, discount) VALUES (%s, %s, %s, %s, %s, %s)"
    item["discount"] = discount(float(item["price_less"][:-1]), float(item["price"][:-1]))
    val = (item["name"], item["ref"], item["description"], item["price_less"][:-1], item["price"][:-1], item["discount"])
    mycursor.execute(sql, val)
print(len(all_products_dict), "record inserted.")

        # Commit du curseur
mydb.commit()