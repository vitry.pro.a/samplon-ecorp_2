from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import mysql.connector

# Initialisation de la liaison avec le site à scraper
url = "http://localhost:8080/level1/"
page = urlopen(url)
html = page.read().decode("utf-8")
document = BeautifulSoup(html, "html.parser")

# Initialisation de la liaison avec la BDD à implémenter
mydb = mysql.connector.connect(
  host="localhost",
  user="antony",
  password="choupette",
  auth_plugin='mysql_native_password',
  database="scrapy"
)
    #Inite du curseur de la BDD
mycursor = mydb.cursor()

# Méthode:
    # I - Chercher les sélecteurs CSS
    # II - Lister les variables
    # III - Inclures les commandes .select dans les varibles
    # les bon sélecteurs CSS
    # IV - Inclures les variables dans un dictionnaire
    # V - Faire une boucle pour lister les différents éléments (prod_HTML)

# Code BS4:
    # Var_BS4 pour la version HTML
all_products_HTML = document.select("div.product")

    # Var pour le tableau
all_products_dict = []

    # Boucle FOR
for prod_HTML in all_products_HTML:
        # Var pour le dictionnaire
    produit = {}
        # Var pour le nom
    produit["nom"] = prod_HTML.select(".product-info h2")[0].text
        # Var pour la ref
    produit["ref"] = prod_HTML.select(".product-info small")[0].text
        # Var pour la description
    produit["description"] = prod_HTML.select(".product-info p")[0].text
        # Var pour le prix
    produit["prix"] = prod_HTML.select(".product-info .line strong.price")[0].text
        # Ajouts du dictionnaire dans le tableau
    all_products_dict.append(produit)

# Code - Création base de donnée SQL:
    # I - Liste des commandes SQL init
            # via fichier SQL

    # II - Via connecteur SQL
# INSERT INTO level_1 (col1, col2, ...) VALUES('valeur1', 'valeur2', ...);

        # Vérification et suppression d'une table existante
table_exist_sql = "DROP TABLE IF EXISTS level_1;"
mycursor.execute(table_exist_sql)
print("Ancienne table level_1: Supprimé")
        # Création de la nouvelle table level_1
table_create_sql = "CREATE TABLE level_1 (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, nom VARCHAR(255), ref VARCHAR(255), description text, prix INT);"
mycursor.execute(table_create_sql)
print("Nouvelle table level_1: Créé")

        # Parcours de la liste "tous les produits dans dictionnaire"
for item in all_products_dict:
    sql = "INSERT INTO level_1 (nom, ref, description, prix) VALUES (%s, %s, %s, %s)"
    val = (item["nom"], item["ref"], item["description"], item["prix"][:-2])
    mycursor.execute(sql, val)
print(len(all_products_dict), "record inserted.")

        # Commit du curseur
mydb.commit()