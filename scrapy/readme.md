# Exo scraping

## installer ce qu'il faut

> Donner l'autorisation d'executer les scripts

```bash
chmod +x install.bash
chmod +x run.bash
```

> Installer nvm, node et eleventy

```bash
./install.bash
```

> lancer le server

```bash
./run.bash
```

Le server est lancé sur :

[http://localhost:8080/level1/](http://localhost:8080/level1/)

Faire `CTRL+C` pour quiter

## A faire
- Créer une BDD avec une ou plusieurs tables pour chaque *level*.
- Récuperer les données demandées
- les mettre dans la BDD
